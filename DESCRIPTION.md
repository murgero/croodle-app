## Croodle

Croodle is an end-to-end encrypted web application to schedule a date or to do a poll on a any topic. All data like title, description, number and labels of options, available answers and names of users and their selections are encrypted/decrypted in the browser using strong 256-bit AES encryption.

### Cron

This app supports running one or more cronjobs. The jobs are specified using the standard crontab syntax.

### ionCube

ionCube is a PHP module extension that loads encrypted PHP files and speeds up webpages. ionCube is pre-installed
and enabled by default.

### Remote Terminal

Use the [web terminal](https://cloudron.io/documentation/apps/#web-terminal) for a remote shell connection into the
app to adjust configuration files like `php.ini`.

