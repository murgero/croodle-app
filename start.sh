#!/bin/bash

set -eu

mkdir -p /app/data/public /run/apache2 /run/cron /run/app/sessions

# generate files if neither index.* or .htaccess
if [[ -z "$(ls -A /app/data/public)" ]]; then
    echo "==> Generate files on first run" # possibly not first run if user deleted index.*
    cp /app/code/index.php /app/data/public/index.php
    echo -e "#!/bin/bash\n\n# Place custom startup commands here" > /app/data/run.sh
    touch /app/data/public/.htaccess
else
    echo "==> Do not override existing index file"
fi

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

# source it so that env vars are persisted
echo "==> Source custom startup script"
[[ -f /app/data/run.sh ]] && source /app/data/run.sh

[[ ! -f /app/data/crontab ]] && cp /app/code/crontab.template /app/data/crontab

## configure in-container Crontab
# http://www.gsp.com/cgi-bin/man.cgi?section=5&topic=crontab
if ! (env; cat /app/data/crontab; echo -e '\nMAILTO=""') | crontab -u www-data -; then
    echo "==> Error importing crontab. Continuing anyway"
else
    echo "==> Imported crontab"
fi

echo "==> Creating credentials.txt"
sed -e "s,\bMAIL_SMTP_SERVER\b,${CLOUDRON_MAIL_SMTP_SERVER}," \
    -e "s,\bMAIL_SMTP_PORT\b,${CLOUDRON_MAIL_SMTP_PORT}," \
    -e "s,\bMAIL_SMTPS_PORT\b,${CLOUDRON_MAIL_SMTPS_PORT}," \
    -e "s,\bMAIL_SMTP_USERNAME\b,${CLOUDRON_MAIL_SMTP_USERNAME}," \
    -e "s,\bMAIL_SMTP_PASSWORD\b,${CLOUDRON_MAIL_SMTP_PASSWORD}," \
    -e "s,\bMAIL_FROM\b,${CLOUDRON_MAIL_FROM}," \
    -e "s,\bMAIL_DOMAIN\b,${CLOUDRON_MAIL_DOMAIN}," \
    -e "s,\bREDIS_HOST\b,${CLOUDRON_REDIS_HOST}," \
    -e "s,\bREDIS_PORT\b,${CLOUDRON_REDIS_PORT}," \
    -e "s,\bREDIS_PASSWORD\b,${CLOUDRON_REDIS_PASSWORD}," \
    -e "s,\bREDIS_URL\b,${CLOUDRON_REDIS_URL}," \
    /app/code/credentials.template > /app/data/credentials.txt

chown -R www-data:www-data /app/data /run/apache2 /run/app /tmp

echo "==> Starting Lamp stack"
exec /usr/bin/supervisord --configuration /etc/supervisor/supervisord.conf --nodaemon -i Lamp
