#!/bin/bash

## Build app
docker build -t mitchellurgero/org.urgero.croodle:latest . --no-cache 
docker push mitchellurgero/org.urgero.croodle:latest

## Install app
cloudron install --image=mitchellurgero/org.urgero.croodle:latest
